#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Pass in a population for k flip bit mutation
*  The parameter k is the exact number of bits to be flipped
*  The parameter sl is the string length
*  The parameter participants is the population that needs mutation
*  Return the mutated individual
*/
int* kFlipBitMutation(int k, int sl, int** participants) {
    if (k <= 0) {
        return *participants;
    }

    int* kTaken = NULL; // Store cross points that have been randomly generated
    kTaken = (int*)malloc(sizeof(int) * k); // Dynamic memory allocation
    if (kTaken == NULL) {
        return *participants;
    }

    int loc;
    bool rep;
    loc = rand() % (sl - 1) + 1; // Randomly generate an bit index
    *kTaken = loc;
    int len_kTaken = 1; // Number of existing cross points
    for (int n = 1; n < k; n++) { // Generate k non repeating bits index and store them in the array

        while (1) { // Add a non repeating bit index after the array kTaken
            rep = false;
            loc = rand() % (sl - 1) + 1; // Randomly generate a bit index index from individual
            for (int i = 0; i < k; i++) {
                if (loc == *(kTaken + i)) {
                    rep = true;
                }
            }

            if (rep == false) { // No duplicate reads detected

                *(kTaken + len_kTaken) = loc;
                len_kTaken++;
                break;
            }
        }
    }

    BubbleSort(kTaken, k); // Sort arrays from small to large

    for (int i = 0; i < k; i++) {
        *(*participants + (*(kTaken+i))) = 1 - *(*participants + (*(kTaken + i))); // The value on the index position of the parent population from kTaken array is inverted
    }

    return *participants;

}