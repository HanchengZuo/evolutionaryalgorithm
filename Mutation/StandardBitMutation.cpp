#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Pass in a population for standard bit mutation
*  The parameter sl is the string length
*  The parameter pm is the mutation probability
*  The parameter participants is the population that needs mutation
*  Return the mutated individual
*/
int* StandardBitMutation(int sl, float pm, int** participants) {
    
    for (int i = 0; i < sl; i++) {
        if ((rand() / double(RAND_MAX)) <= pm) { // When the probability is less or equal than the mutation probability
            *(*participants + i) = 1 - *(*participants + i); // The bit value of the parent individual is inverted
        }
    }

    return *participants;
}