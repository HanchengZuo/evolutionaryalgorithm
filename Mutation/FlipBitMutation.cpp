#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Pass in a population for flip bit mutation
*  The parameter sl is the string length
*  The parameter participants is the population that needs mutation
*  Return the mutated individual
*/
int* FlipBitMutation(int sl, int** participants) {
    int loc = rand() % sl; // Randomly generate a pointer index from individual
    *(*participants + loc) = 1 - *(*participants + loc); // The value on the random index position of the parent population is inverted

    return *participants;

}