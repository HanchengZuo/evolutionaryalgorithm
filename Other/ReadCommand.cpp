#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;


/* Read parameters from the command line and pass them to the program.
*  All parameters are NULL by default
*/
void ReadCommand(int argc, char* argv[],int* fitness, int* mutation, int* crossover, int* pps, int* pops, float* pcp, float* pm, int* it, int* sl, int* kfbm, int* kpc, int* nrp)
{
    // Loop after the first filename parameter
    for (int i = 1; i < argc; i++) {

        const char* fitness_str = "fitness"; // Command line input matching fitness function parameter
        const char* mutation_str = "mutation"; // Command line input matching mutation operator parameter
        const char* crossover_str = "crossover"; // Command line input matching crossover operator parameter

        const char* string_length = "sl"; // Command line input matching string length parameter
        const char* population_size = "ps"; //Command line input matching population size parameter
        const char* offspring_population_size = "ops"; //Command line input matching offspring population size parameter
        const char* crossover_probability = "cp"; //Command line input matching crossover probability parameter
        const char* mutation_probability = "pm"; //Command line input matching mutation probability parameter
        const char* iterations = "it"; //Command line input matching the number of iterations parameter
        const char* k_FlipBitMutation = "kfbm"; // Command line input matching the number k for k Flip Bit Mutation
        const char* k_PointCrossover = "kpc"; // Command line input matching the number k for k Point Crossover
        const char* random_points = "nrp"; // Command line input matching the number of random sampling points

        const char* d = "="; // Equal sign string split
        char* value = strstr(argv[i], d); // Take the value after the equal sign

        // Identify population_size parameter
        char* pagrv = argv[i]; // Take the parameter name for identification
        int len = (int)strlen(population_size); 
        for (int i = 0; i < len; i++) {
            if (*pagrv != *population_size)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *pps;
                }
                pagrv++;
                population_size++;
            }
        }

        // Identify offspring_population_size parameter
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(offspring_population_size);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *offspring_population_size)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *pops;
                }
                pagrv++;
                offspring_population_size++;
            }
        }

        // Identify crossover_probability parameter
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(crossover_probability);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *crossover_probability)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *pcp;
                }
                pagrv++;
                crossover_probability++;
            }
        }

        // Identify mutation_probability parameter
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(mutation_probability);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *mutation_probability)
            {
                
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *pm;
                }
                pagrv++;
                mutation_probability++;
            }
        }

        // Identify iterations parameter
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(iterations);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *iterations)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *it;
                }
                pagrv++;
                iterations++;
            }
        }

        // Identify string_length parameter
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(string_length);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *string_length)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *sl;
                }
                pagrv++;
                string_length++;
            }
        }

        // Identify fitness fuction
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(fitness_str);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *fitness_str)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *fitness;
                }
                pagrv++;
                fitness_str++;
            }
        }

        // Identify mutation fuction
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(mutation_str);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *mutation_str)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *mutation;
                }
                pagrv++;
                mutation_str++;
            }
        }

        // Identify crossover fuction
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(crossover_str);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *crossover_str)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *crossover;
                }
                pagrv++;
                crossover_str++;
            }
        }

        // Identify the number k for k Flip Bit Mutation 
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(k_FlipBitMutation);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *k_FlipBitMutation)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *kfbm;
                }
                pagrv++;
                k_FlipBitMutation++;
            }
        }

        // Identify the number k for k Point Crossover
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(k_PointCrossover);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *k_PointCrossover)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *kpc;
                }
                pagrv++;
                k_PointCrossover++;
            }
        }

        // Identify the number of random sampling points
        pagrv = argv[i]; // Take the parameter name for identification
        len = (int)strlen(random_points);
        for (int i = 0; i < len; i++) {
            if (*pagrv != *random_points)
            {
                break;
            }
            else {
                if (i == len - 1) { // Successfully matched to the corresponding parameter name
                    stringstream strs;
                    strs << value + 1;
                    strs >> *nrp;
                }
                pagrv++;
                random_points++;
            }
        }


    }
}

