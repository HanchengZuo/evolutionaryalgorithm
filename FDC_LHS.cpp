#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
#include "Fitness/Fitness.h"
#include "Fitness/HammingDistance.h"
#include <cmath>
using namespace std;

int main(int argc, char* argv[])
{
    Fitness Fitness;
    HammingDistance HammingDistance;

    initRandomSeed(); // Start random generation function
    //setRandomSeed(1); //Set random number seed and fix each random number generation

    int fitness = 0; // Parameter for running different fitness functions, 0 - Default state, OneMax fitness function, 1 - Leading ones fitness function, 2 - TwoMax fitness function

    int sl = 0; //Length of binary string
    int nrp = 0; // the number of sample points

    // Open a data file to write the results
    FILE* pFile = fopen("data.txt", "w");
    if (pFile == NULL) {
        return -1;
    }

    printf("String length = %d\n", sl);
    printf("The number of random sample points = %d\n\n", nrp);
    fprintf(pFile, "String length = %d\n", sl);
    fprintf(pFile, "The number of random sample points = %d\n\n", nrp);

    ReadCommand(argc, argv, &fitness, NULL, NULL, NULL, NULL, NULL, NULL,NULL, &sl, NULL, NULL, &nrp); // Pass in parameter pointer for modification

    if (nrp > pow(2, sl)) //If nrp is greater than the total number of samples, the default is the total number of samples.
        nrp = pow(2, sl);

    printf("new String length = %d\n", sl);
    printf("new The number of sample points = %d\n\n", nrp);
    fprintf(pFile, "new String length = %d\n", sl);
    fprintf(pFile, "new The number of sample points = %d\n\n", nrp);

    // Print fitness function
    Fitness.printFitnessInfo(fitness, pFile);
    printf("-----------------------------------------------\n\n");
    fprintf(pFile, "-----------------------------------------------\n\n");

    /* Latin hypercube sampling */
    int total = pow(2, sl); // Maximum number of sampling points
    double layers = total / nrp; // Stratified number of samples
    double random; // Generated random number

    int* arr_taken = NULL; // Store used decimal numbers
    arr_taken = (int*)malloc(sizeof(int) * nrp); // Dynamic memory allocation
    if (arr_taken == NULL) {
        return 0;
    }

    int** arr_LHS = NULL; // Store randomly generated sampling points
    arr_LHS = (int**)malloc(sizeof(int*) * nrp); // Dynamic memory allocation
    if (arr_LHS == NULL) {
        return 0;
    }

    int integer_random;
    int length;
    for(int i = 0; i < nrp; i++){

        random = rand()%11; // Take a random number from 0 to 10
        random = random / 10; // Take a random number from  0 to 1
        random = random * layers; // Take a random number from 0 to layers(Enumeration total divided by the specified number of individuals)
        random = random + (i * layers); // The interval of circular mapping random number to actual sample stratification
        integer_random = Round(random); // Round the random number and take the decimal number of individual that has been extracted

        // Loop through the arr_taken array to detect whether there is a integer that has used
        for (int j = 0; j < nrp; j++) {
            if (integer_random == *(arr_taken + j)) { // Duplicate reads detected
                integer_random +=1; // Take an individual on the next position to avoid overlap caused by repeated extraction
            }
        }

        *(arr_taken + i) = integer_random; // Record the taken integer value

        //Convert decimal numbers to binary
        int* arr_integerTostring = NULL; //Used to store the strings after decimal conversion
        arr_integerTostring = (int*)malloc(sizeof(int) * sl); // Dynamic memory allocation
        if (arr_integerTostring == NULL) {
            return 0;
        }

        length = sl - 1; // Add converted binary bits from the tail
        while (integer_random / 2) {
            arr_integerTostring[length--]= integer_random % 2; // Load the remainder obtained by dividing by 2 into the array
            integer_random = integer_random / 2;
        }
        arr_integerTostring[length--] = integer_random % 2; //Store last remainder

        for(int i = length; i >= 0; i--){
            arr_integerTostring[i]= 0; // Add 0 to the previous empty bits
        }

        *(arr_LHS + i) = arr_integerTostring; // Store a random point array to the arr_RandomSampling array

    }

    printf("The samples a given number of random points:\n");
    fprintf(pFile, "The samples a given number of random points:\n");
    for (int i = 0; i < nrp; i++) {
        int** p = arr_LHS + i;
        for (int j = 0; j < sl; j++) {
            printf("%d ", *(*p + j));
            fprintf(pFile, "%d ", *(*p + j));
        }
        printf("   Fitness: %d", Fitness.getFitness(fitness, sl, *p));
        fprintf(pFile, "   Fitness: %d", Fitness.getFitness(fitness, sl, *p));
        printf("   Hamming: %d\n", HammingDistance.getHammingDistance(fitness, sl, *p));
        fprintf(pFile, "   Hamming: %d\n", HammingDistance.getHammingDistance(fitness, sl, *p));
    }
    printf("\n");
    fprintf(pFile, "\n");

    double r;
    r = FDC_calculation(fitness,sl, nrp, arr_LHS);

    printf("r = %.20lf\n", r);
    fprintf(pFile, "r = %.20lf\n", r);
    printf("-----------------------------------------------\n");
    fprintf(pFile, "-----------------------------------------------\n");
    printf("\n");
    fprintf(pFile, "\n");

}
