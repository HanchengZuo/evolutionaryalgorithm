#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Randomly select a number of individuals of size from the population and form a new population
*  The parameter pPopulation is a randomly selected population
*  The parameter sl is the length of the string
*  The parameter ps is the size of population
*  The parameter size determines the size of the new population returned
*/
int** RandomSelection(int** pPopulation, int sl, int ps, int size) {

    int** temPopulation = NULL; // Create a pointer to temporarily store the population
    temPopulation = (int**)malloc(sizeof(int*) * ps);
    if (temPopulation == NULL) {
        return temPopulation;
    }

    // Copy data from pointer pPopulation to temporary pointer, protection of original data
    for (int i = 0; i < ps; i++) {
        int* tem = NULL; // Temporarily store an individual
        tem = (int*)malloc(sizeof(int) * sl);
        if (tem == NULL) {
            return temPopulation;
        }
        for (int j = 0; j < sl; j++) {
            *(tem + j) = *(*(pPopulation + i) + j);
        }
        *(temPopulation + i) = tem;
    }

    int** participants = NULL; // Return new random population
    participants = (int**)malloc(sizeof(int*) * size);
    if (participants == NULL) {
        return participants;
    }

    int* TakenLoc = NULL; // Store pointer indexes that have been read
    TakenLoc = (int*)malloc(sizeof(int) * size);
    if (TakenLoc == NULL) {
        return participants;
    }
    
    int loc;
    bool rep;
    for (int i = 0; i < size; i++) {

        while (1) {
            rep = false; // Initialization repeat as false
            loc = rand() % ps; // Randomly generate a pointer index from pPopulation

            // Loop through the TakenLoc array to detect whether there is a pointer index that has been read
            for (int j = 0; j < size; j++) {
                if (loc == *(TakenLoc + j)) { // Duplicate reads detected
                    rep = true;
                }
            }

            if (rep == false) { // No duplicate reads detected
                break;
            }
        }

        *(participants + i) = *(temPopulation + loc); // Store the random individuals from pPopulation to participants
        *(TakenLoc + i) = loc; // Record the taken pointer index
    }

    return participants;
}