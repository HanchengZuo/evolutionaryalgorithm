#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
#include "Fitness/Fitness_Hamming.h"
#include "Fitness/Fitness.h"
using namespace std;


/* Plus Selection
*  Return the best individuals from the combination of parents and offspring
*  And the same size as ps(Population size)
*/
int** PlusSelection(int** parent_offspring, int fitness, int sl, int ps, int ops) {
    Fitness Fitness;

    int sum = ps + ops; //Number of parents and offspring

    int** temParentOffspring = NULL; // Create a pointer to temporarily store the merged parent and offspring
    temParentOffspring = (int**)malloc(sizeof(int*) * sum);
    if (temParentOffspring == NULL) {
        return temParentOffspring;
    }

    // Copy data from pointer parent_offspring to temporary pointer, protection of original data
    for (int i = 0; i < sum; i++) {
        int* tem = NULL; // Temporarily store an individual
        tem = (int*)malloc(sizeof(int) * sl);
        if (tem == NULL) {
            return temParentOffspring;
        }
        for (int j = 0; j < sl; j++) {
            *(tem + j) = *(*(parent_offspring + i) + j);
        }
        *(temParentOffspring + i) = tem;
    }

    int** participants = NULL;// Return plus selection
    participants = (int**)malloc(sizeof(int*) * ps);// ps indicates the number of best individuals returned
    if (participants == NULL) {
        return participants;
    }

    /* The outer loop determines the number of best individuals returned
    *  The inner loop read the best individual and mark its value as 0, which is equivalent to removing it from the array
    */
    for (int i = 0; i < ps; i++) {
        int max_index = 0; 
        int max_fitness = 0;
        for (int j = 0; j < sum; j++) {
            if (*(temParentOffspring + j) != 0) {

                if (max_fitness < Fitness.getFitness(fitness, sl, *(temParentOffspring + j))) {
                    max_index = j;
                    max_fitness = Fitness.getFitness(fitness, sl, *(temParentOffspring + j));
                }
            }
        }
        *(participants + i) = *(parent_offspring + max_index); // Store the best individuals
        *(temParentOffspring + max_index) = 0; // Mark the value of the best individual as 0 and do not read it again
    }

    return participants;

}