#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
#include "Fitness/Fitness_Hamming.h"
#include "Fitness/Fitness.h"
using namespace std;


/* Tournament Selection
*  Return a population with the k best individuals from the specified population
*  Parameter k determines the optimal number of individuals in the returned population
*  Parameter fitness determines which fitness function to use
*  Parameter size is the size of the specified population
*/
int** TournamentSelection(int k, int** specified_population, int size, int fitness, int sl) {

    Fitness Fitness;

    int** temPopulation = NULL; // Create a pointer to temporarily store the specified population
    temPopulation = (int**)malloc(sizeof(int*) * size);
    if (temPopulation == NULL) {
        return temPopulation;
    }

    // Copy data from pointer specified_population to temporary pointer, protection of original data
    for (int i = 0; i < size; i++) {
        int* tem = NULL; // Temporarily store an individual
        tem = (int*)malloc(sizeof(int) * sl);
        if (tem == NULL) {
            return temPopulation;
        }
        for (int j = 0; j < sl; j++) {
            *(tem + j) = *(*(specified_population + i) + j);
        }
        *(temPopulation + i) = tem;
    }

    int** participants = NULL;// Return tournament selection
    participants = (int**)malloc(sizeof(int*) * k);// k indicates the number of best individuals returned
    if (participants == NULL) {
        return participants;
    }

    /* The outer loop determines the number of best individuals returned
    *  The inner loop read the best individual and mark its value as 0, which is equivalent to removing it from the array
    */
    for (int i = 0; i < k; i++) {
        int max_index = 0;
        int max_fitness = 0;
        for (int j = 0; j < size; j++) {
            if (*(temPopulation + j) != 0) {
                if (max_fitness < Fitness.getFitness(fitness, sl, *(temPopulation + j))) {
                    max_index = j;
                    max_fitness = Fitness.getFitness(fitness, sl, *(temPopulation + j));
                }
            }
        }
        *(participants + i) = *(specified_population + max_index); // Store the best individuals
        *(temPopulation + max_index) = 0; // Mark the value of the best individual as 0 and do not read it again
    }

    return participants;

}