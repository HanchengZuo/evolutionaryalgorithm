#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/EvolutionaryAlgorithm.h"
#include "Fitness/Fitness.h"
#include "Fitness/HammingDistance.h"
#include <cmath>
using namespace std;

int main(int argc, char* argv[])
{
    Fitness Fitness;
    HammingDistance HammingDistance;

    int fitness = 0; // Parameter for running different fitness functions, 0 - Default state, OneMax fitness function, 1 - Leading ones fitness function, 2 - TwoMax fitness function

    int sl = 0; //Length of binary string

    // Open a data file to write the results
    FILE* pFile = fopen("data.txt", "w");
    if (pFile == NULL) {
        return -1;
    }

    printf("String length = %d\n\n", sl);
    fprintf(pFile, "String length = %d\n\n", sl);

	ReadCommand(argc, argv, &fitness, NULL, NULL, NULL, NULL, NULL, NULL,NULL, &sl, NULL, NULL, NULL); // Pass in parameter pointer for modification

    printf("new String length = %d\n\n", sl);
    fprintf(pFile, "new String length = %d\n\n", sl);

    // Print fitness function
    Fitness.printFitnessInfo(fitness, pFile);
    printf("-----------------------------------------------\n\n");
    fprintf(pFile, "-----------------------------------------------\n\n");

    //Fitness Distance Correlation
    int* arr = NULL;
    arr = (int*)malloc(sizeof(int) * sl); // Dynamic memory allocation
    if (arr == NULL) {
        return 0;
    }

    int count_eumerating = sl * pow(2, sl); // Total number of enumerations
    arr_enumerating = (int*)malloc(sizeof(int) * count_eumerating); // Dynamic memory allocation
    if (arr_enumerating == NULL) {
        return 0;
    }
    Enumerating(sl, arr, 0);

    int* individual_arr_enumerating = NULL;
    individual_arr_enumerating = (int*)malloc(sizeof(int) * sl); // Dynamic memory allocation
    if (individual_arr_enumerating == NULL) {
        return 0;
    }

    int** pArr_enumerating = NULL; // Pointer to store arr_enumerating array
    pArr_enumerating = (int**)malloc(sizeof(int*) * pow(2, sl)); // Dynamic memory allocation
    if (pArr_enumerating == NULL) {
        return 0;
    }

    // Copy data from pointer arr_enumerating to pArr_enumerating, separate into individuals by string length
    int count_sl = 0;
    int count_pArr_enumerating = 0;
    for (int i = 0; i < count_eumerating; i++) {

        *(individual_arr_enumerating + count_sl) = *(arr_enumerating + i);
        count_sl += 1;

        if (count_sl == sl) {

            int* tem_arr_enumerating = NULL; // Temporarily store an individual
            tem_arr_enumerating = (int*)malloc(sizeof(int) * sl);
            if (tem_arr_enumerating == NULL) {
                return 0;
            }

            for (int j = 0; j < sl; j++) {
                *(tem_arr_enumerating + j) = *(individual_arr_enumerating + j);
            }
            *(pArr_enumerating + count_pArr_enumerating) = tem_arr_enumerating;
            count_pArr_enumerating += 1;
            count_sl = 0;
        }
    }

    printf("Enumerates a given bit string length:\n");
    fprintf(pFile, "Enumerates a given bit string length:\n");
    for (int i = 0; i < pow(2, sl); i++) {
        int** p = pArr_enumerating + i;
        for (int j = 0; j < sl; j++) {
            printf("%d ", *(*p + j));
            fprintf(pFile, "%d ", *(*p + j));
        }

        printf("   Fitness: %d", Fitness.getFitness(fitness, sl, *p));
        fprintf(pFile, "   Fitness: %d", Fitness.getFitness(fitness, sl, *p));
        printf("   Hamming: %d\n", HammingDistance.getHammingDistance(fitness, sl, *p));
        fprintf(pFile, "   Hamming: %d\n", HammingDistance.getHammingDistance(fitness, sl, *p));
    }
    printf("\n");
    fprintf(pFile, "\n");

    double r;
    r = FDC_calculation(fitness, sl, count_pArr_enumerating, pArr_enumerating);

    printf("r = %.20lf\n", r);
    fprintf(pFile, "r = %.20lf\n",r);
    printf("-----------------------------------------------\n");
    fprintf(pFile, "-----------------------------------------------\n");
    printf("\n");
    fprintf(pFile, "\n");


    return 0;
}