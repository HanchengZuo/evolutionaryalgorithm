#ifndef _EvolutionaryAlgorithm_h
#define _EvolutionaryAlgorithm_h

void ReadCommand(int argc, char* argv[],int* fitness, int* mutation, int* crossover, int* pps, int* pops, float* pcp, float* pm, int* it, int* sl, int* kfbm, int* kpc, int* nrp);

int* CreateIndividual(int sl);

int** CreatePopulation(int sl, int ps);

int OneMax(int sl, int* pIndividual);

int OneMax_HammingDistance(int sl, int* pIndividual);

int LeadingOnes(int sl, int* pIndividual);

int LeadingOnes_HammingDistance(int sl, int* pIndividual);

int TwoMax(int sl, int* pIndividual);

int TwoMax_HammingDistance(int sl, int* pIndividual);

extern int count_arr_enumerating;
extern int* arr_enumerating;
void Enumerating(int sl, int* arr, int i);

int** PlusSelection(int** parent_offspring, int fitness, int sl, int ps, int ops);

int** TournamentSelection(int k, int** specified_population, int size, int fitness, int sl);

int** RandomSelection(int** pPopulation, int sl, int ps, int size);

int* SinglePointCrossover(int sl, int** participants);

int* kPointCrossover(int k, int sl, int** participants);

int* UniformCrossover(int sl, int** participants);

int* FlipBitMutation(int sl, int** participants);

int* kFlipBitMutation(int k, int sl, int** participants);

int* StandardBitMutation(int sl, float pm, int** participants);

int** CreateParentOffspringPopulation(int ps, int ops, int** pPopulation, int** offspring);

void BubbleSort(int* a, int n);

double FDC_calculation(int fitness, int sl, int count_pArr_enumerating, int** pArr_enumerating);

double Round(double r);

#endif