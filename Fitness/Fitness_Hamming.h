#ifndef _FITNESS_HAMMING_H
#define _FITNESS_HAMMING_H


class Fitness_Hamming
{
public:
    virtual int getFitness(int fitness, int sl, int* pIndividual) = 0;
    virtual int getHammingDistance(int fitness, int sl, int* pIndividual) = 0;
    virtual void printFitnessInfo(int fitness, FILE* pFile) = 0;
};

#endif
