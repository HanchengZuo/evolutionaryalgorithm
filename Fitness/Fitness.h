#ifndef _FITNESS_H
#define _FITNESS_H

#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
#include "Fitness_Hamming.h"
using namespace std;

class Fitness: public Fitness_Hamming{
public:
    int getFitness(int fitness, int sl, int* pIndividual)
    {
        int fit = 0;
        switch (fitness) { // Determine which fitness function to use according to the parameter value
            case 0:
                fit = OneMax(sl, pIndividual);
                break;
            case 1:
                fit = LeadingOnes(sl, pIndividual);
                break;
            case 2:
                fit = TwoMax(sl, pIndividual);
                break;
        }
        return fit;
    }

    int getHammingDistance(int fitness, int sl, int* pIndividual){return 0;}

    void printFitnessInfo(int fitness, FILE* pFile)
    {
        // Print fitness function
        switch (fitness) {
            case 0:
                printf("Fitness function = OneMax\n");
                fprintf(pFile, "Fitness function = OneMax\n");
                break;
            case 1:
                printf("Fitness function = Leading ones\n");
                fprintf(pFile, "Fitness function = Leading ones\n");
                break;
            case 2:
                printf("Fitness function = TwoMax\n");
                fprintf(pFile, "Fitness function = TwoMax\n");
                break;

        }
    }
};

#endif
