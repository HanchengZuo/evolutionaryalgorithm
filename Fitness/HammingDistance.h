#ifndef _HammingDistance_H
#define _HammingDistance_H

#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
#include "Fitness_Hamming.h"
using namespace std;

class HammingDistance: public Fitness_Hamming{
public:
    int getFitness(int fitness, int sl, int* pIndividual){return 0;}

    int getHammingDistance(int fitness, int sl, int* pIndividual)
    {
        int dis = 0;
        switch (fitness) { // Determine which HammingDistance function to use according to the parameter value
            case 0:
                dis = OneMax_HammingDistance(sl, pIndividual);
                break;
            case 1:
                dis = LeadingOnes_HammingDistance(sl, pIndividual);
                break;
            case 2:
                dis = TwoMax_HammingDistance(sl, pIndividual);
                break;
        }
        return dis;
    }

    void printFitnessInfo(int fitness, FILE* pFile){}

};

#endif

