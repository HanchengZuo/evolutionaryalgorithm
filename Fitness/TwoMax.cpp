#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Pass in an individual array pointer and return the fitness of the individual
*  The parameter sl is the length of the string
*  The parameter pIndividual is the pointer of a individual array
*  Return the fitness of the individual
*/
int TwoMax(int sl, int* pIndividual) {
    int sum = 0;   // Individual fitness
    int sum_1 = 0; // Individual fitness for bit 1
    int sum_0 = 0; // Individual fitness for bit 2
    for (int i = 0; i < sl; i++)
        sum_1 = sum_1 + *(pIndividual + i); // Iterate over the values of the individual array
    for (int i = 0; i < sl; i++){
        if (*(pIndividual + i) == 0)
            sum_0 += 1;
    }

    if(sum_1 > sum_0){
        sum = sum_1;
    }
    else{
        sum = sum_0;
    }

    return sum;
}

/* Pass in an individual array pointer and return the Hamming distance of a given search point to the closest optimum
*  The parameter sl is the length of the string
*  The parameter pIndividual is the pointer of a individual array
*  Return the Hamming distance of a given search point to the closest optimum
*/
int TwoMax_HammingDistance(int sl, int* pIndividual) {
    int dis = 0; // The Hamming distance of a given search point to the closest optimum
    int dis_0 = 0; // The Hamming distance of a given search point to the 0 bits string
    int dis_1 = 0; // The Hamming distance of a given search point to the 1 bits string

    for (int i = 0; i < sl; i++) {
        if (*(pIndividual + i) != 1) // the bit of an individual and the 1 bits string differ in
            dis_1+=1;
        else if(*(pIndividual + i) != 0) // the bit of an individual and the 0 bits string differ in
            dis_0+=1;

    }

    if (dis_0 > dis_1){
        dis = dis_0;
    }
    else {
        dis = dis_1;
    }

    return dis;
}