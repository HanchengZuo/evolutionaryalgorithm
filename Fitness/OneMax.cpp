#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Pass in an individual array pointer and return the fitness of the individual
*  The parameter sl is the length of the string
*  The parameter pIndividual is the pointer of a individual array
*  Return the fitness of the individual
*/
int OneMax(int sl, int* pIndividual) {
    int sum = 0; // Individual fitness
    for (int i = 0; i < sl; i++)
        sum = sum + *(pIndividual + i); // Iterate over the values of the individual array

    return sum; 
}

/* Pass in an individual array pointer and return the Hamming distance of a given search point to the closest optimum
*  The parameter sl is the length of the string
*  The parameter pIndividual is the pointer of a individual array
*  Return the Hamming distance of a given search point to the closest optimum
*/
int OneMax_HammingDistance(int sl, int* pIndividual) {
    int dis = 0; // The Hamming distance of a given search point to the closest optimum

    for (int i = 0; i < sl; i++) {
        if (*(pIndividual + i) != 1) // the bit of an individual and the optimum differ in
            dis = dis + 1;
    }
    return dis;
}