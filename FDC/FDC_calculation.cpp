#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
#include <cmath>
using namespace std;

// Return the r value of the samples

double FDC_calculation(int fitness, int sl, int size_arr_samples, int** pArr_samples) {

    int* tem_points = NULL;
    tem_points = (int*)malloc(sizeof(int) * sl); // Dynamic memory allocation
    if (tem_points == NULL) {
        return 0;
    }

    int* F = NULL;
    F = (int*)malloc(sizeof(int) * size_arr_samples); // Dynamic memory allocation
    if (F == NULL) {
        return 0;
    }
    int* D = NULL;
    D = (int*)malloc(sizeof(int) * size_arr_samples); // Dynamic memory allocation
    if (D == NULL) {
        return 0;
    }

    for (int i = 0; i < size_arr_samples; i++) {
        int** p = pArr_samples + i;
        for (int j = 0; j < sl; j++) {
            *(tem_points + j) = *(*p + j);
        }
        switch (fitness) { // Determine which fitness function to use according to the parameter value
            case 0: // OneMax fitness function
                *(F + i) = OneMax(sl, tem_points);
                *(D + i) = OneMax_HammingDistance(sl, tem_points);
                break;
            case 1: // Leading Ones Fitness function
                *(F + i) = LeadingOnes(sl, tem_points);
                *(D + i) = LeadingOnes_HammingDistance(sl, tem_points);
                break;
            case 2:// TwoMax Fitness function
                *(F + i) = TwoMax(sl, tem_points);
                *(D + i) = TwoMax_HammingDistance(sl, tem_points);
                break;
        }
    }

    double F_avg;
    for (int k = 0; k < size_arr_samples; k++) {
        F_avg = *(F + k) + F_avg;
    }
    F_avg = F_avg / size_arr_samples;

    double D_avg;
    for (int k = 0; k < size_arr_samples; k++) {
        D_avg = *(D + k) + D_avg;
    }
    D_avg = D_avg / size_arr_samples;

    double c_FD;
    for (int i = 0; i < size_arr_samples; i++) {
        c_FD = (*(F + i) - F_avg) * (*(D + i) - D_avg) + c_FD;

    }
    c_FD = c_FD / size_arr_samples;

    double s_F;
    double c_F;
    double denominator;
    for (int i = 0; i < size_arr_samples; i++) {
        s_F = pow((*(F + i) - F_avg),2) + s_F;
        c_F = pow((*(D + i) - D_avg), 2) + c_F;
    }
    s_F = sqrt(s_F / size_arr_samples);
    c_F = sqrt(c_F / size_arr_samples);
    denominator = s_F * c_F;

    double r;
    r = c_FD / denominator;

    return r;

}