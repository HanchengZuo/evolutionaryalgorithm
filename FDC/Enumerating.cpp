#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"

using namespace std;
using namespace std;

int count_arr_enumerating = 0; // The count variable that stores the enumeration array
int* arr_enumerating = NULL; // Used to store all enumeration data

// Code source: https://www.geeksforgeeks.org/generate-all-the-binary-strings-of-n-bits/
void printTheArray(int sl, int* arr)
{
    for (int i = 0; i < sl; i++) {
        int tem = arr[i];
        *(arr_enumerating + count_arr_enumerating) = tem; // Copy enumeration value to array
        count_arr_enumerating = count_arr_enumerating + 1;
        
    }
}

void Enumerating(int sl, int* arr, int i)
{
    if (i == sl) {
        printTheArray(sl, arr);
        return;
    }

    // First assign "0" at ith position
   // and try for all other permutations
   // for remaining positions
    arr[i] = 0;
    Enumerating(sl, arr, i + 1);

    // And then assign "1" at ith position
    // and try for all other permutations
    // for remaining positions
    arr[i] = 1;
    Enumerating(sl, arr, i + 1);

}
