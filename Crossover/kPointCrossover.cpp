#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Pass in a population for k point crossover
*  The parameter k is the number of cross point
*  The parameter sl is the string length
*  The parameter participants is the population that needs crossover
*  Return the crossovered individual
*/
int* kPointCrossover(int k, int sl, int** participants) {

    if (k <= 0) {
        return *participants;
    }

    int* kTaken = NULL; // Store cross points that have been randomly generated
    kTaken = (int*)malloc(sizeof(int) * k); // Dynamic memory allocation
    if (kTaken == NULL) {
        return *participants;
    }

    int loc;
    bool rep;
    loc = rand() % (sl - 1) + 1; // Randomly generate an cross point index
    *kTaken = loc;
    int len_kTaken = 1; // Number of existing cross points
    for (int n = 1; n < k; n++) { // Generate k non repeating cross points and store them in the array

        while (1) { // Add a non repeating cross point after the array kTaken
            rep = false;
            loc = rand() % (sl - 1) + 1; // Randomly generate a cross point index from individual
            for (int i = 0; i < k; i++) {
                if (loc == *(kTaken + i)) {
                    rep = true;
                }
            }

            if (rep == false) { // No duplicate reads detected

                *(kTaken + len_kTaken) = loc;
                len_kTaken++;
                break;
            }
        }
    }

    BubbleSort(kTaken, k); // Sort arrays from small to large
    
    int n = 0;
    while (n < k) {

        if (k == 1 || n == k - 1) { // Two special cases, if k is 1 or when the index points to the last intersection
            for (int i = *(kTaken + n); i < sl; i++) { // Swap from random index location
                *(*participants + i) = *(*(participants + 1) + i); // The value after the random index position of the first parent is swaped with the second parent
            }
            break;
        }
        for (int i = *(kTaken + n); i < *(kTaken + n + 1); i++) { // Swap from random index location
            *(*participants + i) = *(*(participants + 1) + i); // The value after the random index position of the first parent is swaped with the second parent
        }
        n = n + 2; 
    }

    return *participants;
}