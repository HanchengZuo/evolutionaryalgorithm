#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Pass in a population for uniform crossover
*  The parameter sl is the string length
*  The parameter participants is the population that needs crossover
*  Return the crossovered individual
*/
int* UniformCrossover(int sl, int** participants) {

    float probability = 0.5;
    for (int i = 0; i < sl; i++) {
        if ((rand() / double(RAND_MAX)) <= probability) { // When the probability is less or equal than the probability
            *(*participants + i) = *(*(participants + 1) + i); // The value of the first parent is swaped with the second parent
        }
    }

    return *participants;
}