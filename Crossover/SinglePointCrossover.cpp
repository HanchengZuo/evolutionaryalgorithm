#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Pass in a population for single point crossover
*  The parameter sl is the string length
*  The parameter participants is the population that needs crossover
*  Return the crossovered individual
*/
int* SinglePointCrossover(int sl, int** participants) {
    int loc = rand() % sl; // Randomly generate a pointer index from individual
    for (int i = loc; i < sl; i++) { // Swap from random index location
        *(*participants + i) = *(*(participants + 1) + i); // The value after the random index position of the first parent is swaped with the second parent
    }

    return *participants;
}