﻿#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
#include "Fitness/Fitness.h"
#include "Fitness/HammingDistance.h"
#include <cmath>
using namespace std;

int main(int argc, char* argv[]) 
{
    Fitness Fitness;
    HammingDistance HammingDistance;

    //initRandomSeed(); // Start random generation function
    setRandomSeed(1); //Set random number seed and fix each random number generation

    int fitness = 0; // Parameter for running different fitness functions, 0 - Default state, OneMax fitness function, 1 - Leading ones fitness function, 2 - TwoMax fitness function
    int mutation = 0; // Parameter for running different mutation operators, 0 - Default state, Flip bit mutation operator, 1 - Standard bit mutation operator, 2 - k Flip bit mutation operator
    int crossover = 0; // Parameter for running different crossover operators, 0 - Default state, Single Point Crossover operator, 1 - Uniform Crossover operator, 2 - k Point Crossover
    
    int sl = 0; //Length of binary string
    int ps = 0; //Population size
    int ops = 0; //offspring population size
    float cp = 0.0; //crossover probability
    float pm = 0.0; //Mutation probability
    int it = 0; //the number of iterations
    int kfbm = 0; //k Flip bit mutation
    int kpc = 0; // k Point crossover

    
    // Open a data file to write the results
    FILE* pFile = fopen("data.txt", "w");
    if (pFile == NULL) {
        return -1;
    }

    printf("String length = %d\n", sl);
	printf("Population size = %d\n", ps);
	printf("Offspring population size = %d\n", ops);
    printf("Crossover probability = %.1f\n", cp);
    printf("Mutation probability = %.1f\n", pm);
    printf("The number of iterations = %d\n", it);
    printf("k for flip bit mutation = %d\n", kfbm);
    printf("k for point crossover = %d\n\n", kpc);
    fprintf(pFile, "String length = %d\n", sl);
    fprintf(pFile, "Population size = %d\n", ps);
    fprintf(pFile, "Offspring population size = %d\n", ops);
    fprintf(pFile, "Crossover probability = %.1f\n", cp);
    fprintf(pFile, "Mutation probability = %.1f\n", pm);
    fprintf(pFile, "The number of iterations = %d\n", it);
    fprintf(pFile, "k for flip bit mutation = %d\n", kfbm);
    fprintf(pFile, "k for point crossover = %d\n\n", kpc);

	ReadCommand(argc, argv, &fitness, &mutation, & crossover, &ps, &ops, &cp, &pm, &it, &sl, &kfbm, &kpc, NULL); // Pass in parameter pointer for modification

    printf("new String length = %d\n", sl);
    printf("new Population size = %d\n", ps);
    printf("new Offspring population size = %d\n", ops);
    printf("new Crossover probability = %.1f\n", cp);
    printf("new Mutation probability = %.1f\n", pm);
    printf("new The number of iterations = %d\n", it);
    printf("new k for flip bit mutation = %d\n", kfbm);
    printf("new k for point crossover = %d\n\n", kpc);
    fprintf(pFile, "new String length = %d\n", sl);
    fprintf(pFile, "new Population size = %d\n", ps);
    fprintf(pFile, "new Offspring population size = %d\n", ops);
    fprintf(pFile, "new Crossover probability = %.1f\n", cp);
    fprintf(pFile, "new Mutation probability = %.1f\n", pm);
    fprintf(pFile, "new The number of iterations = %d\n", it);
    fprintf(pFile, "new k for flip bit mutation = %d\n", kfbm);
    fprintf(pFile, "new k for point crossover = %d\n\n", kpc);

    // Print fitness function, mutation operator and crossover operator information
    switch (fitness) {
    case 0:
        printf("Fitness function = OneMax\n");
        fprintf(pFile, "Fitness function = OneMax\n");
        break;
    case 1:
        printf("Fitness function = Leading ones\n");
        fprintf(pFile, "Fitness function = Leading ones\n");
        break;
    case 2:
        printf("Fitness function = TwoMax\n");
        fprintf(pFile, "Fitness function = TwoMax\n");
        break;
    }
    switch (mutation) {
    case 0:
        printf("Mutation operator = Flip bit mutation\n");
        fprintf(pFile, "Mutation operator = Flip bit mutation\n");
        break;
    case 1:
        printf("Mutation operator = Standard bit mutation\n");
        fprintf(pFile, "Mutation operator = Standard bit mutation\n");
        break;
    case 2:
        printf("Mutation operator = k Flip bit mutation\n");
        fprintf(pFile, "Mutation operator = k Flip bit mutation\n");
        break;
    }
    switch (crossover) {
    case 0:
        printf("Crossover operator = Single Point Crossover\n\n");
        fprintf(pFile, "Crossover operator = Single Point Crossover\n\n");
        break;
    case 1:
        printf("Crossover operator = Uniform Crossover\n\n");
        fprintf(pFile, "Crossover operator = Uniform Crossover\n\n");
        break;
    case 2:
        printf("Crossover operator = k Point Crossover\n\n");
        fprintf(pFile, "Crossover operator = k Point Crossover\n\n");
        break;
    }

    printf("-----------------------------------------------\n");
    fprintf(pFile, "-----------------------------------------------\n");
    printf("\n");
    fprintf(pFile, "\n");

    int** pPopulation = CreatePopulation(sl, ps); // Initializing a parent population
    int** best_offspring = NULL; // Initializing a population which choose the best individuals from offspring

    for (int out = 0; out < it; out++) { // Loop based on number of iterations

        //Print generated Population
        printf("Generated Population:\n");
        fprintf(pFile, "Generated Population:\n");
        if (pPopulation != NULL)
        {
            for (int i = 0; i < ps; i++) {
                int** p = pPopulation + i;
                for (int j = 0; j < sl; j++) {
                    printf("%d ", *(*p + j));
                    fprintf(pFile, "%d ", *(*p + j));
                }
                switch (fitness) { // Determine which fitness function to use according to the parameter value
                    case 0: // OneMax fitness function
                        printf("   Fitness: %d", OneMax(sl, *p));
                        fprintf(pFile, "   Fitness: %d", OneMax(sl, *p));
                        printf("   Hamming: %d\n", OneMax_HammingDistance(sl, *p));
                        fprintf(pFile, "   Hamming: %d\n", OneMax_HammingDistance(sl, *p));
                        break;
                    case 1: // Leading Ones Fitness function
                        printf("   Fitness: %d", LeadingOnes(sl, *p));
                        fprintf(pFile, "   Fitness: %d", LeadingOnes(sl, *p));
                        printf("   Hamming: %d\n", LeadingOnes_HammingDistance(sl, *p));
                        fprintf(pFile, "   Hamming: %d\n", LeadingOnes_HammingDistance(sl, *p));
                        break;
                    case 2: // TwoMax Fitness function
                        printf("   Fitness: %d", TwoMax(sl, *p));
                        fprintf(pFile, "   Fitness: %d", TwoMax(sl, *p));
                        printf("   Hamming: %d\n", TwoMax_HammingDistance(sl, *p));
                        fprintf(pFile, "   Hamming: %d\n", TwoMax_HammingDistance(sl, *p));
                        break;

                }
            }
            printf("\n");
            fprintf(pFile,"\n");
        }

        // Run algorithm
        int** offspring = NULL;
        offspring = (int**)malloc(sizeof(int*) * ops);
        if (offspring == NULL)
        {
            return 0;
        }

        for (int i = 0; i < ops; i++) { //Generation of offspring
            if ((rand() / double(RAND_MAX)) < cp) { // Randomly generate a float number between 0 and 1 to compare with the probability
                int** participants = RandomSelection(pPopulation, sl, ps, 2); // Select two random individuals from parent population

                switch (crossover) { // Determine which crossover operator to use according to the parameter value
                    case 0:
                        *(offspring + i) = SinglePointCrossover(sl, participants);
                        break;
                    case 1:
                        *(offspring + i) = UniformCrossover(sl, participants);
                        break;
                    case 2:
                        *(offspring + i) = kPointCrossover(kpc, sl, participants);
                        break;
                }

                switch (mutation) { // Determine which mutation operator to use according to the parameter value
                    case 0:
                        *(offspring + i) = FlipBitMutation(sl, participants);
                        break;
                    case 1:
                        *(offspring + i) = StandardBitMutation(sl, pm, participants);
                        break;
                    case 2:
                        *(offspring + i) = kFlipBitMutation(kfbm, sl, participants);
                        break;
                }
            }
            else {
                int** participants = RandomSelection(pPopulation, sl, ps, 1); // Select one random individual from parent population

                switch (mutation) { // Determine which mutation operator to use according to the parameter value
                    case 0:
                        *(offspring + i) = FlipBitMutation(sl, participants);
                        break;
                    case 1:
                        *(offspring + i) = StandardBitMutation(sl, pm, participants);
                        break;
                    case 2:
                        *(offspring + i) = kFlipBitMutation(kfbm, sl, participants);
                        break;
                }
            }
        }

        //Print generated Offspring
        printf("Generated Offspring:\n");
        fprintf(pFile, "Generated Offspring:\n");
        if (offspring == NULL)
        {
            return 0;
        }
        for (int i = 0; i < ops; i++) {
            int** p = offspring + i;
            for (int j = 0; j < sl; j++) {
                printf("%d ", *(*p + j));
                fprintf(pFile, "%d ", *(*p + j));
            }
            switch (fitness) { // Determine which fitness function to use according to the parameter value
                case 0: // OneMax fitness function
                    printf("   Fitness: %d", OneMax(sl, *p));
                    fprintf(pFile, "   Fitness: %d", OneMax(sl, *p));
                    printf("   Hamming: %d\n", OneMax_HammingDistance(sl, *p));
                    fprintf(pFile, "   Hamming: %d\n", OneMax_HammingDistance(sl, *p));
                    break;
                case 1: // Leading Ones Fitness function
                    printf("   Fitness: %d", LeadingOnes(sl, *p));
                    fprintf(pFile, "   Fitness: %d", LeadingOnes(sl, *p));
                    printf("   Hamming: %d\n", LeadingOnes_HammingDistance(sl, *p));
                    fprintf(pFile, "   Hamming: %d\n", LeadingOnes_HammingDistance(sl, *p));
                    break;
                case 2: // TwoMax Fitness function
                    printf("   Fitness: %d", TwoMax(sl, *p));
                    fprintf(pFile, "   Fitness: %d", TwoMax(sl, *p));
                    printf("   Hamming: %d\n", TwoMax_HammingDistance(sl, *p));
                    fprintf(pFile, "   Hamming: %d\n", TwoMax_HammingDistance(sl, *p));
                    break;

            }
        }
        printf("\n");
        fprintf(pFile, "\n");


        //Find the individual with the highest fitness from the offspring
        printf("The best Offspring:\n");
        fprintf(pFile, "The best Offspring:\n");
        int best_index = 0;
        int best_fitness = 0;
        for (int i = 0; i < ops; i++) { // Looking for the individual with the highest fitness from the offspring
            switch (fitness) { // Determine which fitness function to use according to the parameter value
                case 0: // OneMax fitness function
                    if (best_fitness < OneMax(sl, *(offspring + i))) {
                        best_index = i;
                        best_fitness = OneMax(sl, *(offspring + i));
                    }
                    break;
                case 1: // Leading Ones Fitness function
                    if (best_fitness < LeadingOnes(sl, *(offspring + i))) {
                        best_index = i;
                        best_fitness = LeadingOnes(sl, *(offspring + i));
                    }
                    break;
                case 2: // TwoMax Fitness function
                    if (best_fitness < TwoMax(sl, *(offspring + i))) {
                        best_index = i;
                        best_fitness = TwoMax(sl, *(offspring + i));
                    }
                    break;
            }
        }
        for (int j = 0; j < sl; j++) { // Print the individual with the highest fitness
            printf("%d ", *(*(offspring + best_index) + j));
            fprintf(pFile, "%d ", *(*(offspring + best_index) + j));
        }
        printf("   Fitness: %d\n", Fitness.getFitness(fitness, sl, *(offspring + best_index)));
        fprintf(pFile, "   Fitness: %d\n", Fitness.getFitness(fitness, sl, *(offspring + best_index)));
        printf("\n");
        fprintf(pFile, "\n");

        //Print the best individuals with a population size from the offspring
        best_offspring = TournamentSelection(ps, offspring,ops, fitness, sl); // The population that returns the best individuals with the number of ps from the offspring population
        pPopulation = best_offspring; // Pass the best population to the parent population for the next iteration
        printf("The best from Offspring Population:\n");
        fprintf(pFile, "The best from Offspring Population:\n");
        if (offspring != NULL && best_offspring != NULL)
        {
            for (int i = 0; i < ps; i++) { // Print the best population
                int** p = best_offspring + i;
                for (int j = 0; j < sl; j++) {
                    printf("%d ", *(*p + j));
                    fprintf(pFile, "%d ", *(*p + j));
                }
                printf("   Fitness: %d", Fitness.getFitness(fitness, sl, *p));
                fprintf(pFile, "   Fitness: %d", Fitness.getFitness(fitness, sl, *p));
                printf("   Hamming: %d\n", HammingDistance.getHammingDistance(fitness, sl, *p));
                fprintf(pFile, "   Hamming: %d\n", HammingDistance.getHammingDistance(fitness, sl, *p));
            }
            printf("\n");
            fprintf(pFile, "\n");
        }

        printf("\n");
        printf("-----------------------------------------------");
        printf("\n");
        fprintf(pFile, "\n");
        fprintf(pFile, "-----------------------------------------------");
        fprintf(pFile, "\n");

    }
    free(pPopulation);

	return 0;
}
