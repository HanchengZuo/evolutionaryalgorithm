#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

// Return an int array of individual with a length of String_Length
// The parameter sl is the length of the string
int* CreateIndividual(int sl) {
    int* pIndividual = NULL; // Pointer to store individual array
    pIndividual = (int*)malloc(sizeof(int) * sl); // Dynamic memory allocation

    if (pIndividual == NULL) {
        return pIndividual;
    }

    for (int i = 0; i < sl; i++)
        *(pIndividual + i) = rand() % 2; // Generate a random 0 or 1 number

    return pIndividual;
}