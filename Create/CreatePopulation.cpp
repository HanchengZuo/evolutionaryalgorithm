#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Return an int array of Population with a length of ps(Population size)
/  The parameter sl is the length of the string
/  The parameter ps is the population size
*/
int** CreatePopulation(int sl ,int ps) {
    int** pPopulation = NULL; // Pointer to store population array
    pPopulation = (int**)malloc(sizeof(int*) * ps); // Dynamic memory allocation

    if (pPopulation == NULL) {
        return pPopulation;
    }

    for (int i = 0; i < ps; i++)
        *(pPopulation + i) = CreateIndividual(sl); // Generate a random array of individual to assign to the population array

    return pPopulation;
}