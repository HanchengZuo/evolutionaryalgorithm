#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "HeaderFiles/Random.h"
#include "HeaderFiles/EvolutionaryAlgorithm.h"
using namespace std;

/* Take parent population and offspring population for merging
*  The parameter ps is the population size
*  The parameter ops is the offspring size
*  The parameter pPopulation is the parent population to be merged
*  The parameter offspring is the offspring population to be merged
*  Return the merged population of parent and offspring
*/
int** CreateParentOffspringPopulation(int ps, int ops, int** pPopulation, int** offspring) {
    int sum = ps + ops; // Size of combined population
    int** parent_offspring = NULL; // The pointer of combined population
    parent_offspring = (int**)malloc(sizeof(int*) * sum);
    if (parent_offspring == NULL) {
        return parent_offspring;
    }

    // Add parent population
    for (int i = 0; i < ps; i++)
        *(parent_offspring + i) = *(pPopulation + i);

    // Add offspring population
    for (int i = 0; i < ops; i++)
        *(parent_offspring + ps + i) = *(offspring + i);

    return parent_offspring;
}