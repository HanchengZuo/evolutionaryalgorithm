Version information: FDC_v1.5
1. Adjust Hamming distance function.
2. Create the Fitness_Hamming abstract class, and the corresponding implementation classes are Fitness and Hammingdistance classes.
3. Comment LHS function.
-----------------------------------------------------------------------------------------------

Version information: FDC_v1.4
1. New add FDC_LHS function

-----------------------------------------------------------------------------------------------

Version information: FDC_v1.3
1. Create a new executable file FDC_RandomSampling, the full enumeration and random sampling are separated in separate classes .
2. Random sampling works by randomly generating decimal numbers and converting them into binary numbers.
3. FDC_Enumeration exectable file only has two arguments: fitness and sl.
4. FDC_RandomSampling has three arguments: fitness, sl and nrp.
5. Structure the code a bit better by creating “packages” (folders). 
6. Integrating fitness functions and Hamming distance functions.

-----------------------------------------------------------------------------------------------
Version information: (mu, lambda)EA v1.0

1. Tournament selection is newly added, which provides the function for (mu, lambda) EA algorithm to select the best and the number of population size of individuals from the offspring.

2. The initial version of the (mu, lambda) EA algorithm was successfully implemented.

-----------------------------------------------------------------------------------------------

Version information: FDC_v1.0

1. A version that enumerates all possible search points for a given bit string length.
2.  Calculate the exact value of the fitness-distance correlation.


-----------------------------------------------------------------------------------------------

Version information: FDC_v1.1

1. OneMax.cpp file rename to Main.cpp
2. Update the Hamming distance function to determine the closest optimal value.
3. The new parameter nrp determines the number of random sampling points. If nrp is greater than the total number of samples, the default is the total number of samples.
4. Random sampling by using RandomSelection.cpp function.


-----------------------------------------------------------------------------------------------

Version information: FDC_v1.2

1. To create a new FDC executable file, there are only three command line parameters: fitness=1 sl=3 nrp=6
2. New add TwoMax fitness function
3. Adjust code architecture








