
sl - Length of binary string
ps - Population size
ops - offspring population size
cp - crossover probability（Valid range 0.0 - 1.0）
pm - Mutation probability （Valid range 0.0 - 1.0）
it - the number of iterations
kfbm - k Flip bit mutation
kpc - k Point crossover
nrp - the number of random sample points

fitness - Parameter for running different fitness functions
fitness=0 - Default state, OneMax fitness function
fitness=1 - Leading ones fitness fuction
fitness=2 - TwoMax fitness fuction

mutation - Parameter for running different mutation operators
mutation=0 - Default state, Flip bit mutation operator
mutation=1 - Standard bit mutation operator (The parameter pm for mutation probability)
mutation=2 - k Flip bit mutation operator (The parameter kfbm for k)

crossover - Parameter for running different crossover operators
crossover=0 - Default state, Single Point Crossover operator
crossover=1 - Uniform Crossover operator
crossover=2 - k Point Crossover (The parameter kpc for k)