
For Main:

g++ -std=c++11 BubbleSort.cpp CreateIndividual.cpp CreateParentOffspringPopulation.cpp CreatePopulation.cpp Enumerating.cpp FDC_calculation.cpp FlipBitMutation.cpp kFlipBitMutation.cpp kPointCrossover.cpp LeadingOnes.cpp Main.cpp OneMax.cpp EvolutionaryAlgorithm.h PlusSelection.cpp Random.cpp Random.h RandomPoints.cpp RandomSelection.cpp ReadCommand.cpp SinglePointCrossover.cpp StandardBitMutation.cpp TournamentSelection.cpp UniformCrossover.cpp TwoMax.cpp -o onemax

./onemax fitness=1 mutation=2 crossover=2 ps=5 ops=10 cp=0.6 pm=0.3 it=3 sl=5 kfbm=3 kpc=3


For FDC:

g++ -std=c++11 BubbleSort.cpp CreateIndividual.cpp CreateParentOffspringPopulation.cpp CreatePopulation.cpp Enumerating.cpp FDC_calculation.cpp FlipBitMutation.cpp kFlipBitMutation.cpp kPointCrossover.cpp LeadingOnes.cpp FDC_Main.cpp OneMax.cpp EvolutionaryAlgorithm.h PlusSelection.cpp Random.cpp Random.h RandomPoints.cpp RandomSelection.cpp ReadCommand.cpp SinglePointCrossover.cpp StandardBitMutation.cpp TournamentSelection.cpp UniformCrossover.cpp TwoMax.cpp -o onemax

./onemax fitness=1 sl=3 nrp=6